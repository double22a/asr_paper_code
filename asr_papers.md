# ASR Paper

## transformer

- [Transformers with convolutional context for ASR](https://arxiv.org/abs/1904.11660), Abdelrahman Mohamed et al, 1904 

- [Conformer: Convolution-augmented Transformer for Speech Recognition](https://arxiv.org/abs/2005.08100), Anmol Gulati et al, 2005

- [SIMPLIFIED SELF-ATTENTION FOR TRANSFORMER-BASED END-TO-END SPEECH RECOGNITION](https://arxiv.org/abs/2005.10463), Haoneng Luo et al, 2005

- [Advanced Long-context End-to-end Speech Recognition Using Context-expanded Transformers](https://arxiv.org/abs/2104.09426), Takaaki Hori et al, 2104

- [A Survey of Transformers](https://arxiv.org/abs/2106.04554), Tianyang Lin et al, 2106

## transducer

- [TRANSFORMER-TRANSDUCER: END-TO-END SPEECH RECOGNITION WITH SELF-ATTENTION](https://arxiv.org/abs/1910.12977), Ching-Feng Yeh et al, 1910

- [IMPROVING ACCURACY OF RARE WORDS FOR RNN-TRANSDUCER THROUGH UNIGRAM SHALLOW FUSION](https://arxiv.org/abs/2012.00133), Vijay Ravi et al, 2012

- [LESS IS MORE: IMPROVED RNN-T DECODING USING LIMITED LABEL CONTEXT AND PATH MERGING](https://arxiv.org/abs/2012.06749), Rohit Prabhavalkar et al, 2012

## streaming

- [A BETTER AND FASTER END-TO-END MODEL FOR STREAMING ASR](https://arxiv.org/abs/2011.10798), Bo Li et al, 2011

- [Bridging the gap between streaming and non-streaming ASR systems by distilling ensembles of CTC and RNN-T models](https://arxiv.org/abs/2104.14346), Thibault Doutre et al, 2104

- [Reducing Streaming ASR Model Delay with Self Alignment](https://arxiv.org/abs/2105.05005), Jaeyoung Kim et al, 2105

- [Streaming End-to-End ASR based on Blockwise Non-Autoregressive Models](https://arxiv.org/abs/2107.09428), Tianzi Wang et al, 2107


## cascaded

- [Modular End-to-end Automatic Speech Recognition Framework for Acoustic-to-word Model](https://arxiv.org/pdf/2008.00953.pdf), Qi Liu et al, 2008

- [TRANSFORMER TRANSDUCER: ONE MODEL UNIFYING STREAMING AND NON-STREAMING SPEECH RECOGNITION](https://arxiv.org/abs/2010.03192), Anshuman Tripathi et al, 2010

- [UNIVERSAL ASR: UNIFYING STREAMING AND NON-STREAMING ASR USING A SINGLE ENCODER-DECODER MODEL](https://arxiv.org/abs/2010.14099), Zhifu Gao et al, 2010

- [CASCADED ENCODERS FOR UNIFYING STREAMING AND NON-STREAMING ASR](https://arxiv.org/abs/2010.14606), Arun Narayanan et al, 2010

- [CASCADE RNN-TRANSDUCER: SYLLABLE BASED STREAMING ON-DEVICE MANDARIN SPEECH RECOGNITION WITH A SYLLABLE-TO-CHARACTER CONVERTER](https://arxiv.org/abs/2011.08469), Xiong Wang et al, 2011

- [Unified Streaming and Non-streaming Two-pass End-to-end Model for Speech Recognition](https://arxiv.org/abs/2012.05481), Binbin Zhang, 2012

- [TRANSFORMER BASED DELIBERATION FOR TWO-PASS SPEECH RECOGNITION](https://arxiv.org/abs/2101.11577), Ke Hu et al, 2101

- [TSNAT: Two-Step Non-Autoregressvie Transformer Models for Speech Recognition](https://arxiv.org/abs/2104.01522), Zhengkun Tian et al, 2104

- [DECOUPLING RECOGNITION AND TRANSCRIPTION IN MANDARIN ASR](https://arxiv.org/abs/2108.01129), Jiahong Yuan et al, 2108

## contextual

- [HYBRID AUTOREGRESSIVE TRANSDUCER (HAT)](https://arxiv.org/abs/2003.07705), Ehsan Variani et al, 2003

- [Contextualized Streaming End-to-End Speech Recognition with Trie-Based Deep Biasing and Shallow Fusion](https://arxiv.org/abs/2104.02194), Duc Le et al, 2104

- [Tree-constrained Pointer Generator for End-to-end Contextual Speech Recognition](https://arxiv.org/abs/2109.00627), Guangzhi Sun et al, 2109

## multilingual

- [Scaling End-to-End Models for Large-Scale Multilingual ASR](https://arxiv.org/abs/2104.14830), Bo Li et al, 2104

- [A Study of Multilingual End-to-End Speech Recognition for Kazakh, Russian, and English](https://arxiv.org/abs/2108.01280), Saida Mussakhojayeva et al, 2108

- [Multilingual Speech Recognition for Low-Resource Indian Languages using Multi-Task conformer](https://arxiv.org/abs/2109.03969), Krishna D N, 2109
